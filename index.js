// Load the modules
var express = require("express");
var path = require("path");

// Create the application instance
var app = express();

// *** Define the routes ***

// Static routes
app.use(express.static(path.join(__dirname,"public")));
app.use(express.static(path.join(__dirname,"bower_components")));

// *** End of routes ***

// Set the application port
app.set("port",process.argv[2]||process.env.APP_PORT||3000);

// Listen to the port
app.listen(app.get("port"), function() {
    console.log("Application started on port %s", app.get("port"));
});